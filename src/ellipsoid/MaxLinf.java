package ellipsoid;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * MaxLinf uses a MIQCP model to maximize the L-infinity norm of any point in
 * the feasible region.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MaxLinf extends MIQCP {

  /**
   * Constructor.
   * @param constraint the constraint defining the ellipsoid
   * @throws IloException if the model cannot be built
   */
  public MaxLinf(final Constraint constraint) throws IloException {
    super(constraint);
    // Set the objective to maximize the L-infinity norm of x.
    cplex.addMaximize(y);
  }

  /**
   * Solves the model.
   * @param timeLimit the time limit in seconds for the run
   * @return the final solver status
   * @throws IloException if CPLEX blows up
   */
  public IloCplex.Status solve(final double timeLimit) throws IloException {
    // Set the run time limit.
    cplex.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    // Eliminate log output.
    cplex.setOut(null);
    // Try to solve the model.
    cplex.solve();
    // Return the final status.
    return cplex.getStatus();
  }

  /**
   * Gets the final objective value.
   * @return the objective value
   * @throws IloException if there is no objective value
   */
  public double getObjValue() throws IloException {
    return cplex.getObjValue();
  }
}
