package ellipsoid;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 * MaxL1 uses a MIQCP model to maximize the L1 norm of any point in the
 * feasible region.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MaxL1 extends MIQCP {
  private final IloNumVar[] absX;  // absolute values of x variables

  /**
   * Constructor.
   * @param constraint the constraint defining the ellipsoid
   * @throws IloException if the model cannot be built
   */
  public MaxL1(final Constraint constraint) throws IloException {
    super(constraint);
    // Remove the y, z and w variables (and associated constraints).
    cplex.remove(y);
    cplex.remove(z);
    cplex.remove(w);
    // Add variables to capture the absolute values of the x variables,
    // along with the corresponding constraints.
    absX = new IloNumVar[constraint.getDimension()];
    for (int i = 0; i < constraint.getDimension(); i++) {
      absX[i] = cplex.numVar(0, Double.MAX_VALUE, "absX_" + i);
      cplex.addEq(cplex.abs(x[i]), absX[i]);
    }
    // Set the objective to maximize the L-1 norm of x.
    cplex.addMaximize(cplex.sum(absX));
  }

  /**
   * Solves the model.
   * @param timeLimit the time limit in seconds for the run
   * @return the final solver status
   * @throws IloException if CPLEX blows up
   */
  public IloCplex.Status solve(final double timeLimit) throws IloException {
    // Set the run time limit.
    cplex.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    // Eliminate log output.
    cplex.setOut(null);
    // Try to solve the model.
    cplex.solve();
    // Return the final status.
    return cplex.getStatus();
  }

  /**
   * Gets the final objective value.
   * @return the objective value
   * @throws IloException if there is no objective value
   */
  public double getObjValue() throws IloException {
    return cplex.getObjValue();
  }
}
