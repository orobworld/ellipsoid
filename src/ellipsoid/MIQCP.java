package ellipsoid;

import ilog.concert.IloException;
import ilog.concert.IloLQNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 * MIQCP contains the common elements of MIQCP models for determining if the
 * feasible region is bounded.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public abstract class MIQCP implements AutoCloseable {
  protected final IloCplex cplex;  // the MIQPC model
  protected final IloNumVar[] x;   // the primary variables
  protected final IloNumVar y;     // y will contain the L-infinity norm of x
  protected final IloNumVar[] z;   // z[i] = 1 => y = x[i]
  protected final IloNumVar[] w;   // w[i] = 1 => y = x[i]

  /**
   * Constructor.
   * @param constraint the constraint defining the ellipsoid
   * @throws IloException if the model cannot be built
   */
  public MIQCP(final Constraint constraint) throws IloException {
    int dimension = constraint.getDimension();
    // Initialize the model.
    cplex = new IloCplex();
    // Define the variables.
    x = new IloNumVar[dimension];
    z = new IloNumVar[dimension];
    w = new IloNumVar[dimension];
    y = cplex.numVar(0, Double.MAX_VALUE, "y");
    for (int i = 0; i < dimension; i++) {
      x[i] = cplex.numVar(-Double.MAX_VALUE, Double.MAX_VALUE, "x_" + i);
      z[i] = cplex.boolVar("z_" + i);
      w[i] = cplex.boolVar("w_" + i);
    }
    // We leave the choice of objective function to the subclass.
    // We want exactly one indicator to be 1.
    cplex.addEq(cplex.sum(cplex.sum(z), cplex.sum(w)), 1.0);
    // y is at least the absolute value of every x variable.
    for (int i = 0; i < dimension; i++) {
      cplex.addGe(y, x[i]);
      cplex.addGe(y, cplex.prod(-1.0, x[i]));
    }
    // We use implication constraints to tie z, w, y and x together.
    for (int i = 0; i < dimension; i++) {
      cplex.add(cplex.ifThen(cplex.eq(z[i], 1.0), cplex.eq(y, x[i])));
      cplex.add(cplex.ifThen(cplex.eq(w[i], 1.0),
                             cplex.eq(y, cplex.prod(-1.0, x[i]))));
    }
    // Create the quadratic constraint.
    double[][] qMat = constraint.getqMat();
    double[] q = constraint.getQ();
    double q0 = constraint.getQ0();
    IloLQNumExpr lhs = cplex.lqNumExpr();
    for (int row = 0; row < dimension; row++) {
      for (int col = 0; col < dimension; col++) {
        lhs.addTerm(qMat[row][col], x[row], x[col]);
      }
      lhs.addTerm(q[row], x[row]);
    }
    cplex.addLe(lhs, -q0);
  }

  /**
   * Closes the model.
   */
  @Override
  public final void close() {
    cplex.end();
  }

}
