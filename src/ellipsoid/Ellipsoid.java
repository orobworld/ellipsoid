package ellipsoid;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * Ellipsoid experiments with a randomly generated ellipsoid to see if it is
 * bounded or unbounded.
 *
 * The motivation is a question on OR Stack Exchange:
 * https://or.stackexchange.com/questions/10579/randomly-constructing-a
 * -bounded-ellipsoid
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Ellipsoid {

  /** Dummy constructor. */
  private Ellipsoid() { }

  /**
   * Generates a test problem and tries multiple optimization models.
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set the dimension of the ellipsoid, a random number seed, and a time
    // limit in seconds for every solver run.
    int dim = 5;
//    long seed = 61623;
    long seed = 20230616;
//    long seed = 12345;
    double timeLimit = 60;
    // For the expansion model, set an initial lower bound and an upper limit
    // on the lower bound.
    double lb = 100;
    double maxLB = 5000;
    // Generate the test problem.
    Constraint constraint = new Constraint(dim, seed);
//    System.out.println(constraint);
    // Try the MaxLinf model to see if the L-infinity norm of all feasible
    // points is bounded.
    System.out.println("Running the L-infinity maximiation model ...");
    try (MaxLinf model = new MaxLinf(constraint)) {
      IloCplex.Status status = model.solve(timeLimit);
      System.out.println("Final status = " + status + ".");
      if (status == IloCplex.Status.Optimal) {
        System.out.println("Maximum L-infinity norm = " + model.getObjValue());
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    // Try the MaxL1 model to see if the L1 norm of all feasible points is
    // bounded.
    System.out.println("Running the L1 maximiation model ...");
    try (MaxL1 model = new MaxL1(constraint)) {
      IloCplex.Status status = model.solve(timeLimit);
      System.out.println("Final status = " + status + ".");
      if (status == IloCplex.Status.Optimal) {
        System.out.println("Maximum L1 norm = " + model.getObjValue());
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    // Try the expansion model.
    System.out.println("\nRunning the expansion model ...");
    try (ExpansionModel model = new ExpansionModel(constraint)) {
      IloCplex.Status status = model.solve(lb, maxLB, timeLimit);
      if (status == IloCplex.Status.Optimal) {
        System.out.println("Failed to find an upper bound!");
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
  }

}
