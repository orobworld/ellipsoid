package ellipsoid;

import java.util.Arrays;
import java.util.Random;

/**
 * Constraint contains a random instance of the quadratic constraint defining
 * the ellipsoid.
 *
 * We follow the notation from the OR SE post with one change: the factor 1/2
 * is absorbed into the Q matrix.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Constraint {
  private static final double HALF = 0.5;
  private final double[][] qMat;  // the Q matrix
  private final double[] q;       // the q vector
  private final double q0;        // the constant term q0
  private final int dimension;    // the problem dimension

  /**
   * Constructs a random instance of the constraint.
   * @param dim the dimension (number of variables)
   * @param seed the seed for the random number generator
   */
  public Constraint(final int dim, final long seed) {
    dimension = dim;
    // Seed a random number generator.
    Random rng = new Random(seed);
    // Allocate the matrices.
    qMat = new double[dimension][dimension];
    q = new double[dimension];
    // Generate the matrix used to produce the Q matrix.
    double[][] factor = new double[dimension][dimension];
    for (int r = 0; r < dimension; r++) {
      for (int c = 0; c < dimension; c++) {
        factor[r][c] = rng.nextGaussian();
      }
    }
    // Create the Q matrix (equal to factor' * factor) and multiply by 1/2.
    for (int row = 0; row < dimension; row++) {
      for (int col = 0; col < dimension; col++) {
        double sum = 0;
        for (int d = 0; d < dimension; d++) {
          sum += factor[d][row] * factor[d][col];
        }
        qMat[row][col] = HALF * sum;
      }
    }
    // Create the q vector.
    for (int i = 0; i < dimension; i++) {
      q[i] = rng.nextGaussian();
    }
    // Generate q0.
    q0 = rng.nextGaussian() - dimension;
  }

  /**
   * Gets the Q matrix.
   * @return the Q matrix
   */
  public double[][] getqMat() {
    double[][] m = new double[qMat.length][];
    for (int row = 0; row < qMat.length; row++) {
      m[row] = Arrays.copyOf(qMat[row], qMat.length);
    }
    return m;
  }

  /**
   * Gets the q vector.
   * @return the q vector
   */
  public double[] getQ() {
    return Arrays.copyOf(q, q.length);
  }

  /**
   * Gets the constant term q0.
   * @return q0
   */
  public double getQ0() {
    return q0;
  }

  /**
   * Gets the problem dimension.
   * @return the dimension
   */
  public int getDimension() {
    return dimension;
  }

  /**
   * Displays the constraint elements.
   * @return a string listing the constraint elements
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Constraint elements:\nQ =\n");
    for (double[] row : qMat) {
      sb.append(Arrays.toString(row)).append("\n");
    }
    sb.append("q = ").append(Arrays.toString(q))
      .append("\nq0 = ").append(q0).append("\n");
    return sb.toString();
  }

}
