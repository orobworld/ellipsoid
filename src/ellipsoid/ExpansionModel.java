package ellipsoid;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * ExpansionModel tests whether the ellipsoid is bounded by verifying
 * feasibility with progressively larger lower limits on the L-infinity norm of
 * x.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class ExpansionModel extends MIQCP {

  /**
   * Constructor.
   * @param constraint the constraint defining the ellipsoid
   * @throws IloException if the model cannot be built
   */
  public ExpansionModel(final Constraint constraint)
         throws IloException {
    super(constraint);
    // Leave the default objective (minimize 0). The solve command will set a
    // lower bound on y.
  }

  /**
   * Solves the model.
   * @param lb the lower bound on the L-infinity norm.
   * @param ub the upper bound on the L-infinity norm
   * @param timeLimit the time limit in seconds per search iteration
   * @return the final solver status
   * @throws IloException if CPLEX blows up
   */
  public IloCplex.Status solve(final double lb, double ub,
                               final double timeLimit)
                         throws IloException {
    double lowBound = lb;
    // Set the run time limit.
    cplex.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    // Eliminate log output.
    cplex.setOut(null);
    while (lowBound < ub) {
      // Set the lower bound on the L-infinity norm.
      y.setLB(lowBound);
      // Try to solve the model.
      cplex.solve();
      // Get the solver status.
      IloCplex.Status status = cplex.getStatus();
      if (status == IloCplex.Status.Infeasible) {
        System.out.println("Cannot find a feasible solution with L-infinity"
                           + " norm at least " + lowBound + ".");
        break;
      } else {
        lowBound *= 2.0;
      }
    }
    // Return the final status.
    return cplex.getStatus();
  }
}
