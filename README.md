# Bounded or Unbounded Ellipsoid

This repository contains a Java program investigates whether a randomly generated ellipsoid is bounded or unbounded. The source of the problem is a [question](https://or.stackexchange.com/questions/10579/randomly-constructing-a-bounded-ellipsoid) on Operations Research Stack Exchange.

In the [first](https://orinanobworld.blogspot.com/2023/06/an-unbounded-bounded-feasible-region-i.html) of two blog posts, I give a mathematical argument why (with probability 1) the ellipsoid should be bounded.In the [second post](https://orinanobworld.blogspot.com/2023/06/an-unbounded-bounded-feasible-region-ii.html), I test several random instances (dimension 5) using the code here. As described in the second post, the results are inconsistent. It may be that some mathematical issue is causing problems for CPLEX.

The program requires CPLEX (tested with version 22.1.1) but is otherwise self-contained.